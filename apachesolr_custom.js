jQuery(document).ready(function(){
    jQuery('#block-apachesolr-search-sort > ul').each(function(){
    var list=jQuery(this);
    select=jQuery(document.createElement('select')).addClass('apachesolr-sort-selectbox').insertAfter(jQuery('.page-search .search-description')).change(function(){window.open(jQuery(this).val(),'_self')});

     var newest = jQuery(document.createElement('option')).appendTo(select).val('').html('Sort ...');
     jQuery('>li a', this).each(function(){

      if (this.href.indexOf('asc') !== -1) {
        var order = 'asc &#8593;';
      }
      if (this.href.indexOf('desc') !== -1) {
        var order = 'desc &#x2193;';
      }

      var option=jQuery(document.createElement('option'))
       .appendTo(select)
       .val(this.href)
       .html(jQuery(this).html() + ' (' + order + ')');
      if(window.location.href === this.href){
        option.attr('selected','selected');
      }                                   
    });                                   
    
//    var newest = jQuery(document.createElement('option')).appendTo(select).val(window.location.href + '?solrsort=ds_created%20desc').html('Newest');
//    var oldest = jQuery(document.createElement('option')).appendTo(select).val(window.location.href + '?solrsort=ds_created%20asc').html('Oldest');

    list.remove();                        
     jQuery('#block-apachesolr-search-sort').hide();   
     });                                   
}); 

